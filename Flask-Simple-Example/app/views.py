# -*- coding: utf-8 -*-

from flask import render_template
from app import app, db
from .models import Person
from .forms import PersonForm


@app.route('/', methods = ['GET', 'POST'])
def index():
    form = PersonForm()

    history = Person.query.limit(10)

    if form.validate_on_submit():
        person = Person(name = form.name.data)
        db.session.add(person)
        db.session.commit()

        return render_template('index.html', name = form.name.data, history = history)

    return render_template('index.html', form = form, history = history)